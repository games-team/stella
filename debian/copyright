Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: stella
Source: https://stella-emu.github.io
Copyright: 1995-2021 Bradford W. Mott, Stephen Anthony and the Stella Team
License: GPL-2+
Files-Excluded: test/roms

Files: *
Copyright: 1995-2024 Bradford W. Mott, Stephen Anthony and the Stella
		     Team
License: GPL-2+

Files: debian/*
Copyright: 1998-2004 Tom Lear <tom@trap.mtview.ca.us>
	   2006 Mario Iseli <admin@marioiseli.com>
	   2010-2022, 2024 Stephen Kitt <skitt@debian.org>
License: GPL-2+

Files:
 src/common/Stack.hxx
 src/emucore/FrameBuffer.hxx
 src/emucore/FSNode.*
 src/gui/*
Copyright: 2002-2004 The ScummVM project
License: GPL-2+

Files: src/common/tv_filters/AtariNTSC.hxx
Copyright: 2006-2009 Shay Green
License: LGPL-2.1+

Files: src/common/ZipHandler.hxx
Copyright: Aaron Giles
License: BSD-3
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above
       copyright notice, this list of conditions and the following
       disclaimer in the documentation and/or other materials provided
       with the distribution.
     * Neither the name 'MAME' nor the names of its contributors may
       be used to endorse or promote products derived from this
       software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY AARON GILES ''AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL AARON GILES BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: src/emucore/MD5.*
Copyright: 1991-1992, RSA Data Security, Inc.
License: RSA
 License to copy and use this software is granted provided that it is
 identified as the "RSA Data Security, Inc. MD5 Message-Digest
 Algorithm" in all material mentioning or referencing this software or
 this function.
 .
 License is also granted to make and use derivative works provided
 that such works are identified as "derived from the RSA Data
 Security, Inc. MD5 Message-Digest Algorithm" in all material
 mentioning or referencing the derived work.
 .
 RSA Data Security, Inc. makes no representations concerning either
 the merchantability of this software or the suitability of this
 software for any particular purpose. It is provided "as is" without
 express or implied warranty of any kind.
 .
 These notices must be retained in any copies of any part of this
 documentation and/or software.

Files: src/gui/Stella*tFont.hxx
Copyright: 2010-2014 Dimitar Toshkov Zhekov
License: OFL-1.1
 Terminus Font is licensed under the SIL Open Font License, Version
 1.1.
 .
 Copyright (c) 2010-2014 Dimitar Toshkov Zhekov,
 with Reserved Font Name "Terminus Font".
 .
 To avoid name conflicts with other products named "Terminus", the
 archive should be distributed as "Terminus Font", not "Terminus".
 .
 -----------------------------------------------------------
 SIL OPEN FONT LICENSE Version 1.1 - 26 February 2007
 -----------------------------------------------------------
 .
 PREAMBLE
 The goals of the Open Font License (OFL) are to stimulate worldwide
 development of collaborative font projects, to support the font
 creation efforts of academic and linguistic communities, and to
 provide a free and open framework in which fonts may be shared and
 improved in partnership with others.
 .
 The OFL allows the licensed fonts to be used, studied, modified and
 redistributed freely as long as they are not sold by themselves. The
 fonts, including any derivative works, can be bundled, embedded,
 redistributed and/or sold with any software provided that any
 reserved names are not used by derivative works. The fonts and
 derivatives, however, cannot be released under any other type of
 license. The requirement for fonts to remain under this license does
 not apply to any document created using the fonts or their
 derivatives.
 .
 DEFINITIONS
 "Font Software" refers to the set of files released by the Copyright
 Holder(s) under this license and clearly marked as such. This may
 include source files, build scripts and documentation.
 .
 "Reserved Font Name" refers to any names specified as such after the
 copyright statement(s).
 .
 "Original Version" refers to the collection of Font Software
 components as distributed by the Copyright Holder(s).
 .
 "Modified Version" refers to any derivative made by adding to,
 deleting, or substituting -- in part or in whole -- any of the
 components of the Original Version, by changing formats or by porting
 the Font Software to a new environment.
 .
 "Author" refers to any designer, engineer, programmer, technical
 writer or other person who contributed to the Font Software.
 .
 PERMISSION & CONDITIONS
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of the Font Software, to use, study, copy, merge, embed,
 modify, redistribute, and sell modified and unmodified copies of the
 Font Software, subject to the following conditions:
 .
 1) Neither the Font Software nor any of its individual components,
 in Original or Modified Versions, may be sold by itself.
 .
 2) Original or Modified Versions of the Font Software may be bundled,
 redistributed and/or sold with any software, provided that each copy
 contains the above copyright notice and this license. These can be
 included either as stand-alone text files, human-readable headers or
 in the appropriate machine-readable metadata fields within text or
 binary files as long as those fields can be easily viewed by the
 user.
 .
 3) No Modified Version of the Font Software may use the Reserved Font
 Name(s) unless explicit written permission is granted by the
 corresponding Copyright Holder. This restriction only applies to the
 primary font name as presented to the users.
 .
 4) The name(s) of the Copyright Holder(s) or the Author(s) of the
 Font Software shall not be used to promote, endorse or advertise any
 Modified Version, except to acknowledge the contribution(s) of the
 Copyright Holder(s) and the Author(s) or with their explicit written
 permission.
 .
 5) The Font Software, modified or unmodified, in part or in whole,
 must be distributed entirely under this license, and must not be
 distributed under any other license. The requirement for fonts to
 remain under this license does not apply to any document created
 using the Font Software.
 .
 TERMINATION
 This license becomes null and void if any of the above conditions are
 not met.
 .
 DISCLAIMER
 THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL THE
 COPYRIGHT HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM
 OTHER DEALINGS IN THE FONT SOFTWARE.

Files: src/lib/httplib/httplib.h
Copyright: 2024 Yuji Hirose
License: MIT

Files: src/lib/json/*
Copyright: 2013-2023 Niels Lohmann
           2008-2009 Bjoern Hoehrmann
           2009 Florian Loitsch
License: MIT

Files: src/lib/libpng/*
Copyright: 1995-1996 Guy Eric Schalnat, Group 42, Inc.
           1995-2020 The PNG Reference Library Authors
	   1996-1997 Andreas Dilger
	   1998-2013 Glenn Randers-Pehrson
           2018-2024 Cosmin Truta
License: libpng
 The PNG Reference Library is supplied "AS IS".  The Contributing
 Authors and Group 42, Inc. disclaim all warranties, expressed or
 implied, including, without limitation, the warranties of
 merchantability and of fitness for any purpose.  The Contributing
 Authors and Group 42, Inc. assume no liability for direct, indirect,
 incidental, special, exemplary, or consequential damages, which may
 result from the use of the PNG Reference Library, even if advised of
 the possibility of such damage.
 .
 Permission is hereby granted to use, copy, modify, and distribute
 this source code, or portions hereof, for any purpose, without fee,
 subject to the following restrictions:
 .
   1. The origin of this source code must not be misrepresented.
 .
   2. Altered versions must be plainly marked as such and must not be
      misrepresented as being the original source.
 .
   3. This Copyright notice may not be removed or altered from any
      source or altered source distribution.
 .
 The Contributing Authors and Group 42, Inc. specifically permit,
 without fee, and encourage the use of this source code as a component
 to supporting the PNG file format in commercial products.  If you use
 this source code in a product, acknowledgment is not required but
 would be appreciated.
 .
 There is no warranty against interference with your enjoyment of the
 library or against infringement.  There is no warranty that our
 efforts or the library will fulfill any of your particular purposes
 or needs.  This library is provided with all faults, and the entire
 risk of satisfactory quality, performance, accuracy, and effort is
 with the user.

Files: src/lib/nanojpeg/*
Copyright: 2009-2016 Martin J. Fiedler
License: MIT

Files: src/lib/sqlite/source/*
Copyright: none
License: sqlite
 The author disclaims copyright to this source code.  In place of
 a legal notice, here is a blessing:
 .
    May you do good and not evil.
    May you find forgiveness for yourself and forgive others.
    May you share freely, never taking more than you give.

Files: src/lib/tinyexif/*
Copyright: 2015-2017 Seacave
License: BSD-2
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY EXPRESS
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 NO EVENT SHALL THE FREEBSD PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: src/lib/zlib/*
Copyright: 1995-2024 Jean-loup Gailly and Mark Adler
License: zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any
 damages arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute
 it freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must
    not claim that you wrote the original software. If you use this
    software in a product, an acknowledgment in the product
    documentation would be appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must
    not be misrepresented as being the original software.
 3. This notice may not be removed or altered from any source
    distribution.

Files: src/os/libretro/libretro.h
Copyright: 2010-2020 The RetroArch team
License: MIT

Files: src/os/macos/*
Copyright: 2005-2006 Mark Grebe
License: GPL-2+

Files: src/tools/convbdf.c
Copyright: 2002 Greg Haerr
License: GPL-2+

Files: src/tools/evdev-joystick/*
Copyright: 2016 Stephen Anthony
License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License, version 2, as
 published by the Free Software Foundation.
 .
 This package is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 USA.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in
 `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at
 your option) any later version.
 .
 This package is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 USA.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in
 `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 This module is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2.1 of the
 License, or (at your option) any later version.
 .
 This module is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this module; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301 USA.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in
 `/usr/share/common-licenses/LGPL-2.1'.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
